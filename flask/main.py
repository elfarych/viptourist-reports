from flask import Flask
from flask import request, send_from_directory
from flask_cors import CORS

import orders_excel_report_creator, payments_excel_report_creator

app = Flask(__name__)
CORS(app)
app.debug = True


@app.route('/orders-excel-report', methods=['POST'])
def orders():
    if request.method == 'POST':
        data = request.json
        orders_excel_report_creator.create_excel_report(data)

        return send_from_directory('reports', as_attachment=True, path=f'{data["title"]}.xlsx')


@app.route('/payments-excel-report', methods=['POST'])
def payments():
    if request.method == 'POST':
        data = request.json
        payments_excel_report_creator.create_excel_report(data)

        return send_from_directory('payments', as_attachment=True, path=f'{data["title"]}.xlsx')


if __name__ == '__main__':
    app.run(host='37.140.241.144', port=3000)
