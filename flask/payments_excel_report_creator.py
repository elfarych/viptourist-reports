import xlsxwriter
import locale

locale.setlocale(locale.LC_ALL, '')


def create_excel_report(data):
    # открываем новый файл на запись
    workbook = xlsxwriter.Workbook(f'payments/{data["title"]}.xlsx')

    # создаем "лист"
    worksheet = workbook.add_worksheet()
    worksheet.set_landscape()
    subtitle = workbook.add_format({'bold': True, 'font_size': 12})
    title = workbook.add_format({'bold': True, 'font_size': 16})
    bold = workbook.add_format({'bold': True, 'align': 'right', 'border': True})
    table_title = workbook.add_format(
        {'bold': True, 'align': 'right', 'border': True, 'bg_color': '#81b541', 'font_color': 'white'}
    )

    # Заголовок документа
    worksheet.write(0, 0, 'VIPTOURIST FINANCE', title)
    worksheet.write(2, 0, data['title'], subtitle)
    worksheet.write(3, 0, f'Период: {data["dateFrom"]} - {data["dateTo"]}', subtitle)

    # Заголовки таблицы
    worksheet.write(5, 0, 'Id выплаты', table_title)
    worksheet.write(5, 1, 'Id профиля', table_title)
    worksheet.write(5, 2, 'Создан', table_title)
    worksheet.write(5, 3, 'Сумма', table_title)
    worksheet.write(5, 4, 'Статус', table_title)

    # Парсинг данных таблицы
    create_orders_data(data['payments'], worksheet, workbook)

    worksheet.set_column(0, 0, 25)
    worksheet.set_column(1, 1, 25)
    worksheet.set_column(2, 4, 15)

    # сохраняем и закрываем
    workbook.close()


def create_orders_data(payments, worksheet, workbook):
    summary = 0
    count = 0
    commission_sum = 0

    # Formats
    totals_format = workbook.add_format({'bold': True, 'font_size': 12, 'bg_color': '#e0e0e0', 'border': True})
    data_format = workbook.add_format({'border': True, 'align': 'right'})
    warning_data_format = workbook.add_format(
        {'border': True, 'font_color': 'white', 'bg_color': 'red', 'align': 'right'})
    success_data_format = workbook.add_format(
        {'border': True, 'font_color': 'white', 'bg_color': 'green', 'align': 'right'})

    # Orders
    for payment in payments:
        sum = '{0:n} $'.format(payment['sum'])
        status = ''
        status_format = ''

        if payment['completed']:
            status = 'Выплачен'
            status_format = success_data_format

        elif payment['canceled']:
            status = 'Отклонен'
            status_format = warning_data_format

        else:
            status = 'В ожидани'
            status_format = data_format

        date = payment['createdAt'].split('T')[0].split('-')
        date.reverse()
        date = '.'.join(date) + 'г.'

        worksheet.write(count + 6, 0, payment['id'], data_format)
        worksheet.write(count + 6, 1, payment['profile']['id'], data_format)
        worksheet.write(count + 6, 2, date, data_format)
        worksheet.write(count + 6, 3, sum, data_format)
        worksheet.write(count + 6, 4, status, status_format)

        count += 1
        summary += int(payment['sum'])

    # Totals
    worksheet.write(count + 7, 0, 'ИТОГО:', totals_format)
    worksheet.write(count + 8, 0, f'Кол-во выплат: {count}', totals_format)
    worksheet.write(count + 9, 0, 'На общую сумму: {0:n} $'.format(summary), totals_format)
