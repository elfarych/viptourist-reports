import xlsxwriter
import locale

locale.setlocale(locale.LC_ALL, '')


def create_excel_report(data):
    # открываем новый файл на запись
    workbook = xlsxwriter.Workbook(f'reports/{data["title"]}.xlsx')

    # создаем "лист"
    worksheet = workbook.add_worksheet()
    worksheet.set_landscape()
    subtitle = workbook.add_format({'bold': True, 'font_size': 12})
    title = workbook.add_format({'bold': True, 'font_size': 16})
    bold = workbook.add_format({'bold': True, 'align': 'right', 'border': True})
    table_title = workbook.add_format(
        {'bold': True, 'align': 'right', 'border': True, 'bg_color': '#81b541', 'font_color': 'white'}
    )

    # Заголовок документа
    worksheet.write(0, 0, 'VIPTOURIST FINANCE', title)
    worksheet.write(2, 0, data['title'], subtitle)
    worksheet.write(3, 0, f'Период: {data["dateFrom"]} - {data["dateTo"]}', subtitle)

    # Заголовки таблицы
    worksheet.write(5, 0, 'Id', table_title)
    worksheet.write(5, 1, 'Создан', table_title)
    worksheet.write(5, 2, 'Цена', table_title)
    worksheet.write(5, 3, 'Комиссия', table_title)
    worksheet.write(5, 4, 'Подтв. продавцом', table_title)
    worksheet.write(5, 5, 'Активирован', table_title)
    worksheet.write(5, 6, 'Отменен', table_title)

    # Парсинг данных таблицы
    create_orders_data(data['orders'], worksheet, workbook)

    worksheet.set_column(0, 0, 30)
    worksheet.set_column(1, 3, 15)
    worksheet.set_column(4, 4, 20)
    worksheet.set_column(5, 6, 15)

    # сохраняем и закрываем
    workbook.close()


def create_orders_data(orders, worksheet, workbook):
    summary = 0
    count = 0
    commission_sum = 0

    # Formats
    totals_format = workbook.add_format({'bold': True, 'font_size': 12, 'bg_color': '#e0e0e0', 'border': True})
    data_format = workbook.add_format({'border': True, 'align': 'right'})
    warning_data_format = workbook.add_format(
        {'border': True, 'font_color': 'white', 'bg_color': 'red', 'align': 'right'})
    success_data_format = workbook.add_format(
        {'border': True, 'font_color': 'white', 'bg_color': 'green', 'align': 'right'})

    # Orders
    for order in orders:
        price = '{0:n} $'.format(order['price'])
        commission = '{0:n}$'.format(order['commission'])

        date = order['createdAt'].split('T')[0].split('-')
        date.reverse()
        date = '.'.join(date) + 'г.'

        worksheet.write(count + 6, 0, order['id'], data_format)
        worksheet.write(count + 6, 1, date, data_format)
        worksheet.write(count + 6, 2, price, data_format)
        worksheet.write(count + 6, 3, commission, data_format)
        worksheet.write(count + 6, 4, 'Да' if order['seller_confirmed'] else 'Нет', data_format)
        worksheet.write(count + 6, 5, 'Да' if order['activated'] else 'Нет',
                        success_data_format if order['activated'] else data_format)
        worksheet.write(count + 6, 6, 'Да' if order['canceled'] else 'Нет',
                        warning_data_format if order['canceled'] else data_format)
        count += 1
        summary += int(order['price'])
        commission_sum += int(order['commission'])

    # Totals
    worksheet.write(count + 7, 0, 'ИТОГО:', totals_format)
    worksheet.write(count + 8, 0, f'Кол-во заказов: {count}', totals_format)
    worksheet.write(count + 9, 0, 'На общую сумму: {0:n} $'.format(summary), totals_format)
    worksheet.write(count + 10, 0, 'Комиссионные: {0:n} $'.format(commission_sum), totals_format)
